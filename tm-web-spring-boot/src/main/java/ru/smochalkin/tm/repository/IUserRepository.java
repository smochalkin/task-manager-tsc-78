package ru.smochalkin.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.smochalkin.tm.model.User;

public interface IUserRepository extends JpaRepository<User, String> {

    User findByLogin(final String login);

    User findByEmail(final String email);

    void deleteByLogin(final String login);

}
