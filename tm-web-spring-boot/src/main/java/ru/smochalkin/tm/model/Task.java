package ru.smochalkin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
public class Task extends AbstractBusinessEntity {

    @Column(name = "project_id")
    private String projectId;

    public Task(final String name, final String description) {
        this.name = name;
        this.description = description;
    }

}
