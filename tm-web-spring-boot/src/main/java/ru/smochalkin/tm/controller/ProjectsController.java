package ru.smochalkin.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.smochalkin.tm.api.service.IProjectService;
import ru.smochalkin.tm.model.CustomUser;

@Controller
public class ProjectsController {

    @Autowired
    private IProjectService projectService;

    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    @GetMapping("/projects")
    public ModelAndView index(@AuthenticationPrincipal(errorOnInvalidType = true) final CustomUser user) {
        return new ModelAndView("project-list", "projects", projectService.findAll(user.getUserId()));
    }

    @ModelAttribute("view_name")
    public String getViewName() {
        return "Projects";
    }

}
