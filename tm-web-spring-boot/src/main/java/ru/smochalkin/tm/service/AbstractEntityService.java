package ru.smochalkin.tm.service;

import ru.smochalkin.tm.api.service.IEntityService;
import ru.smochalkin.tm.model.AbstractEntity;

public abstract class AbstractEntityService<E extends AbstractEntity> implements IEntityService<E> {
}
